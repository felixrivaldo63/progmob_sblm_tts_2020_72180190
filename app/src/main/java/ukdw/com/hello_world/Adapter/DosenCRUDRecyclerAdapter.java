package ukdw.com.hello_world.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.hello_world.Model.Dosen;
import ukdw.com.hello_world.R;
import ukdw.com.hello_world.UTS.CrudDosen.UtsUpdateDosenActivity;
import ukdw.com.hello_world.UTS.CrudMhs.UtsUpdateMhsActivity;

public class DosenCRUDRecyclerAdapter extends RecyclerView.Adapter<DosenCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Dosen> dosenList;

    public DosenCRUDRecyclerAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }

    public DosenCRUDRecyclerAdapter(List<Dosen> dosenList) {
        this.dosenList = dosenList;
    }

    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_dosen,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Dosen d = dosenList.get(position);
        holder.tvNama.setText(d.getNama());
        holder.tvNidn.setText(d.getNidn());
        holder.tvAlamat.setText(d.getAlamat());
        holder.tvEmail.setText(d.getEmail());
        holder.tvGelar.setText(d.getGelar());
        holder.d = d;
        //holder.tvNoTelp.setText(m.getNotelp());
    }

    @Override
    public int getItemCount() {
        return dosenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama, tvNidn, rvGetDosenAllUts, tvAlamat, tvEmail, tvGelar;
        Dosen d;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNamaDsn);
            tvNidn = itemView.findViewById(R.id.tvNidnDsn);
            tvAlamat = itemView.findViewById(R.id.tvAlamatDsn);
            tvEmail = itemView.findViewById(R.id.tvEmailDsn);
            tvGelar = itemView.findViewById(R.id.tvGelarDsn);
            rvGetDosenAllUts = itemView.findViewById(R.id.rvGetDosenAllUts);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentUpdate = new Intent(itemView.getContext(), UtsUpdateDosenActivity.class);
                    intentUpdate.putExtra("nama",d.getNama());
                    intentUpdate.putExtra("nidn",d.getNidn());
                    intentUpdate.putExtra("alamat",d.getAlamat());
                    intentUpdate.putExtra("email",d.getEmail());
                    intentUpdate.putExtra("gelar",d.getGelar());
                    Bundle b = new Bundle();
                    intentUpdate.putExtras(b);
                    itemView.getContext().startActivity(intentUpdate);
                }
            });
        }
    }

}
