package ukdw.com.hello_world.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.hello_world.Model.Dosen;
import ukdw.com.hello_world.Model.Matkul;
import ukdw.com.hello_world.R;
import ukdw.com.hello_world.UTS.CrudDosen.UtsUpdateDosenActivity;
import ukdw.com.hello_world.UTS.CrudMatakuliah.UtsUpdateMatkulActivity;

public class MatkulCRUDRecyclerAdapter extends RecyclerView.Adapter<MatkulCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Matkul> matkulList = new ArrayList<>();

    public MatkulCRUDRecyclerAdapter(Context context) {
        this.context = context;
        matkulList = new ArrayList<>();
    }

    public MatkulCRUDRecyclerAdapter(List<Matkul> matkulList) {
        this.matkulList = matkulList;
    }

    public List<Matkul> getMatkulList() {
        return matkulList;
    }

    public void setMatkulList(List<Matkul> matkulList) {
        this.matkulList = matkulList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_matkul,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Matkul m = matkulList.get(position);
        holder.tvKodeMatkul.setText(m.getKode());
        holder.tvNamaMatkul.setText(m.getNama());
        holder.tvHariMatkul.setText(m.getHari().toString());
        holder.tvSesiMatkul.setText(m.getSesi().toString());
        holder.tvSksMatkul.setText(m.getSks().toString());
        holder.m = m;
        //holder.tvNoTelp.setText(m.getNotelp());
    }

    @Override
    public int getItemCount() {
        return matkulList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvKodeMatkul, tvNamaMatkul, rvGetMatkulAllUts, tvHariMatkul, tvSesiMatkul, tvSksMatkul;
        Matkul m;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvKodeMatkul = itemView.findViewById(R.id.tvKodeMatkul);
            tvNamaMatkul = itemView.findViewById(R.id.tvNamaMatkul);
            tvHariMatkul = itemView.findViewById(R.id.tvHariMatkul);
            tvSesiMatkul = itemView.findViewById(R.id.tvSesiMatkul);
            tvSksMatkul = itemView.findViewById(R.id.tvSksMatkul);
            rvGetMatkulAllUts = itemView.findViewById(R.id.rvGetMatkulAllUts);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentUpdate = new Intent(itemView.getContext(), UtsUpdateMatkulActivity.class);
                    intentUpdate.putExtra("nama",m.getNama());
                    intentUpdate.putExtra("kode",m.getKode());
                    intentUpdate.putExtra("hari",m.getHari().toString());
                    intentUpdate.putExtra("sesi",m.getSesi().toString());
                    intentUpdate.putExtra("sks",m.getSks().toString());
                    Bundle b = new Bundle();
                    intentUpdate.putExtras(b);
                    itemView.getContext().startActivity(intentUpdate);
                }
            });
        }
    }

}
