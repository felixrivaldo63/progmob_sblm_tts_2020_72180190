package ukdw.com.hello_world.UTS;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import ukdw.com.hello_world.R;
import ukdw.com.hello_world.UTS.CrudDosen.UtsMainDosenActivity;
import ukdw.com.hello_world.UTS.CrudMatakuliah.UtsAddMatkulActivity;
import ukdw.com.hello_world.UTS.CrudMatakuliah.UtsDeleteMatkulActivity;
import ukdw.com.hello_world.UTS.CrudMatakuliah.UtsMainMatkulActivity;
import ukdw.com.hello_world.UTS.CrudMhs.UtsMainMhsActivity;

public class HomeActivity extends AppCompatActivity {
    String isLogin = "";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logoutHome:
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        HomeActivity.this
                );
                builder.setIcon(R.drawable.ic_warning);
                builder.setTitle("Logout");
                builder.setMessage("Apakah Anda Ingin Logout?");

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void logout(){
        SharedPreferences sharedPreferences = HomeActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor =  sharedPreferences.edit();
        isLogin = sharedPreferences.getString("isLogin","0");
        editor.putString("isLogin","0");
        sharedPreferences.edit().clear().commit();
        Intent intent = new Intent(this,MasukActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        androidx.appcompat.widget.Toolbar myToolbarMhs = (Toolbar) findViewById(R.id.toolbarHome);
        setSupportActionBar(myToolbarMhs);

        ImageView imgMhs = (ImageView) findViewById(R.id.imgDataMhs);
        ImageView imgDsn = (ImageView) findViewById(R.id.imgDataDosen);
        ImageView imgMatkul = (ImageView) findViewById(R.id.imgDataMatkul);

        imgMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, UtsMainMhsActivity.class);
                startActivity(intent);
            }
        });

        imgDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, UtsMainDosenActivity.class);
                startActivity(intent);
            }
        });

        imgMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, UtsMainMatkulActivity.class);
                startActivity(intent);
            }
        });


    }
}