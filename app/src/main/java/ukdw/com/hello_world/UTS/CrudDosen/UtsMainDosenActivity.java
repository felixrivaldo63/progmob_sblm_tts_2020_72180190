package ukdw.com.hello_world.UTS.CrudDosen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.hello_world.Adapter.DosenCRUDRecyclerAdapter;
import ukdw.com.hello_world.Adapter.MahasiswaCRUDRecyclerAdapter;
import ukdw.com.hello_world.Model.Dosen;
import ukdw.com.hello_world.Model.Mahasiswa;
import ukdw.com.hello_world.Network.GetDataService;
import ukdw.com.hello_world.Network.RetrofitClientInstance;
import ukdw.com.hello_world.R;
import ukdw.com.hello_world.UTS.CrudMhs.UtsAddMhsActivity;
import ukdw.com.hello_world.UTS.CrudMhs.UtsDeleteMhsActivity;
import ukdw.com.hello_world.UTS.CrudMhs.UtsMainMhsActivity;
import ukdw.com.hello_world.UTS.HomeActivity;
import ukdw.com.hello_world.UTS.MasukActivity;

public class UtsMainDosenActivity extends AppCompatActivity {
    RecyclerView rvDsn;
    DosenCRUDRecyclerAdapter DsnAdapter;
    ProgressDialog pd;
    List<Dosen> dosenList;
    String isLogin = "";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_mhs, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void logout(){
        SharedPreferences sharedPreferences = UtsMainDosenActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor =  sharedPreferences.edit();
        isLogin = sharedPreferences.getString("isLogin","0");
        editor.putString("isLogin","0");
        sharedPreferences.edit().clear().commit();
        Intent intent = new Intent(this,MasukActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addUts:
                Intent intentAdd = new Intent(this, UtsAddDosenActivity.class);
                startActivity(intentAdd);
                return true;
            case R.id.delUts:
                Intent intentDel = new Intent(this, UtsDeleteDosenActivity.class);
                startActivity(intentDel);
                return true;
            case R.id.backHomeUts:
                Intent intentBack = new Intent(this, HomeActivity.class);
                intentBack.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentBack);
                return true;
            case R.id.logoutBtn:
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        UtsMainDosenActivity.this
                );
                builder.setIcon(R.drawable.ic_warning);
                builder.setTitle("Logout");
                builder.setMessage("Apakah Anda Ingin Logout?");

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_main_dosen);

        androidx.appcompat.widget.Toolbar myToolbarMhs = (Toolbar) findViewById(R.id.toolbarMainDosenUts);
        setSupportActionBar(myToolbarMhs);

        rvDsn = (RecyclerView) findViewById(R.id.rvGetDosenAllUts);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Dosen>> call = service.getDosen("72180190");

        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response) {
                pd.dismiss();
                dosenList = response.body();
                DsnAdapter = new DosenCRUDRecyclerAdapter(dosenList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(UtsMainDosenActivity.this);
                rvDsn.setLayoutManager(layoutManager);
                rvDsn.setAdapter(DsnAdapter);
            }
            @Override
            public void onFailure(Call<List<Dosen>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(UtsMainDosenActivity.this, "Error", Toast.LENGTH_LONG);
            }
        });
    }
}