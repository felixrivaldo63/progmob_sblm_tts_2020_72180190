package ukdw.com.hello_world.UTS.CrudMhs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.hello_world.Adapter.MahasiswaCRUDRecyclerAdapter;
import ukdw.com.hello_world.Model.Mahasiswa;
import ukdw.com.hello_world.Network.GetDataService;
import ukdw.com.hello_world.Network.RetrofitClientInstance;
import ukdw.com.hello_world.R;
import ukdw.com.hello_world.UTS.CrudDosen.UtsDeleteDosenActivity;
import ukdw.com.hello_world.UTS.CrudMatakuliah.UtsMainMatkulActivity;
import ukdw.com.hello_world.UTS.HomeActivity;
import ukdw.com.hello_world.UTS.MasukActivity;

public class UtsMainMhsActivity extends AppCompatActivity {
    RecyclerView rvMhs;
    MahasiswaCRUDRecyclerAdapter mhsAdapter;
    ProgressDialog pd;
    List<Mahasiswa> mahasiswaList;
    String isLogin = "";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_mhs, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addUts:
                Intent intentAdd = new Intent(this, UtsAddMhsActivity.class);
                startActivity(intentAdd);
                return true;
            case R.id.delUts:
                Intent intentDel = new Intent(this, UtsDeleteMhsActivity.class);
                startActivity(intentDel);
                return true;
            case R.id.backHomeUts:
                Intent intentBack = new Intent(this, HomeActivity.class);
                intentBack.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentBack);
                return true;
            case R.id.logoutBtn:
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        UtsMainMhsActivity.this
                );
                builder.setIcon(R.drawable.ic_warning);
                builder.setTitle("Logout");
                builder.setMessage("Apakah Anda Ingin Logout?");

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logout(){
        SharedPreferences sharedPreferences = UtsMainMhsActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor =  sharedPreferences.edit();
        isLogin = sharedPreferences.getString("isLogin","0");
        editor.putString("isLogin","0");
        sharedPreferences.edit().clear().commit();
        Intent intent = new Intent(this,MasukActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

        @Override
        protected void onCreate (Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_uts_main_mhs);

            androidx.appcompat.widget.Toolbar myToolbarMhs = (Toolbar) findViewById(R.id.toolbarMainMhs);
            setSupportActionBar(myToolbarMhs);


            rvMhs = (RecyclerView) findViewById(R.id.rvGetMhsAllUts);
            pd = new ProgressDialog(this);
            pd.setTitle("Mohon Bersabar");
            pd.show();

            GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
            Call<List<Mahasiswa>> call = service.getMahasiswa("72180190");

            call.enqueue(new Callback<List<Mahasiswa>>() {
                @Override
                public void onResponse(Call<List<Mahasiswa>> call, Response<List<Mahasiswa>> response) {
                    pd.dismiss();
                    mahasiswaList = response.body();
                    mhsAdapter = new MahasiswaCRUDRecyclerAdapter(mahasiswaList);

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(UtsMainMhsActivity.this);
                    rvMhs.setLayoutManager(layoutManager);
                    rvMhs.setAdapter(mhsAdapter);

                }

                @Override
                public void onFailure(Call<List<Mahasiswa>> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(UtsMainMhsActivity.this, "Error", Toast.LENGTH_LONG);
                }
            });
        }
    }