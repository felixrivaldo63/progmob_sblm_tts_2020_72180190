package ukdw.com.hello_world.UTS.CrudDosen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.hello_world.Model.DefaultResult;
import ukdw.com.hello_world.Network.GetDataService;
import ukdw.com.hello_world.Network.RetrofitClientInstance;
import ukdw.com.hello_world.R;
import ukdw.com.hello_world.UTS.CrudMhs.UtsAddMhsActivity;
import ukdw.com.hello_world.UTS.CrudMhs.UtsMainMhsActivity;

public class UtsAddDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cancle, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cancleCRUD:
                Intent intentCancle = new Intent(this, UtsMainDosenActivity.class);
                startActivity(intentCancle);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_add_dosen);

        androidx.appcompat.widget.Toolbar myToolbarMhs = (Toolbar) findViewById(R.id.toolbarAddDsnUts);
        setSupportActionBar(myToolbarMhs);

        EditText edNamaUts = (EditText) findViewById(R.id.editTextNamaDosenUts);
        EditText edNidnUts = (EditText) findViewById(R.id.editTextNidnDosenUts);
        EditText edAlamatUts = (EditText) findViewById(R.id.editTextAlamatDosenUts);
        EditText edEmailUts = (EditText) findViewById(R.id.editTextEmailDosenUts);
        EditText edGelarUts = (EditText) findViewById(R.id.editTextGelarDosenUts);
        Button btnSimpanDsnUts = (Button) findViewById(R.id.buttonSimpanDosenUts);
        pd = new ProgressDialog(UtsAddDosenActivity.this);

        btnSimpanDsnUts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dosen(
                        edNamaUts.getText().toString(),
                        edNidnUts.getText().toString(),
                        edAlamatUts.getText().toString(),
                        edEmailUts.getText().toString(),
                        edGelarUts.getText().toString(),
                        "kososngkan saja diisi sembarang karena dirandom sistem",
                        "72180190"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UtsAddDosenActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                        Intent intentAddData = new Intent(UtsAddDosenActivity.this, UtsMainDosenActivity.class);
                        startActivity(intentAddData);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UtsAddDosenActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}