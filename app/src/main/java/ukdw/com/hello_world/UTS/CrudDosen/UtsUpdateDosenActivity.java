package ukdw.com.hello_world.UTS.CrudDosen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.hello_world.Model.DefaultResult;
import ukdw.com.hello_world.Network.GetDataService;
import ukdw.com.hello_world.Network.RetrofitClientInstance;
import ukdw.com.hello_world.R;
import ukdw.com.hello_world.UTS.CrudMatakuliah.UtsUpdateMatkulActivity;
import ukdw.com.hello_world.UTS.CrudMhs.UtsMainMhsActivity;
import ukdw.com.hello_world.UTS.CrudMhs.UtsUpdateMhsActivity;
import ukdw.com.hello_world.UTS.HomeActivity;

public class UtsUpdateDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cancle, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cancleCRUD:
                Intent intentCancle = new Intent(this, UtsMainDosenActivity.class);
                startActivity(intentCancle);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_update_dosen);

        androidx.appcompat.widget.Toolbar myToolbarMhs = (Toolbar) findViewById(R.id.toolbarUpdateDosenUts);
        setSupportActionBar(myToolbarMhs);

        EditText edNidnCari = (EditText) findViewById(R.id.editTextNidnCariDosenUts);
        EditText edNamaBaru = (EditText) findViewById(R.id.editTextNamaBaruDosenUts);
        EditText edNidnBaru = (EditText) findViewById(R.id.editTextNidnBaruDosenUts);
        EditText edAlamatBaru = (EditText) findViewById(R.id.editTextAlamatBaruDosenUts);
        EditText edEmailBaru = (EditText) findViewById(R.id.editTextEmailBaruDosenUts);
        EditText edGelarBaru = (EditText) findViewById(R.id.editTextGelarBaruDosenUts);
        Button btnUpdate = (Button) findViewById(R.id.buttonUpdateDosenUts);
        pd = new ProgressDialog(UtsUpdateDosenActivity.this);
        Intent data = getIntent();

        edNamaBaru.setText(data.getStringExtra("nama"));
        edNidnBaru.setText(data.getStringExtra("nidn"));
        edNidnCari.setText(data.getStringExtra("nidn"));
        edAlamatBaru.setText(data.getStringExtra("alamat"));
        edEmailBaru.setText(data.getStringExtra("email"));
        edGelarBaru.setText(data.getStringExtra("gelar"));

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Ditunggu...");
                pd.setCancelable(false);
                pd.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_dosen(
                        edNamaBaru.getText().toString(),
                        edNidnBaru.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        "kososngkan saja diisi sembarang karena dirandom sistem",
                        "72180190",
                        edNidnCari.getText().toString()
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UtsUpdateDosenActivity.this, "Data Berhasil di Ubah", Toast.LENGTH_LONG).show();
                        Intent intentUpdateData = new Intent(UtsUpdateDosenActivity.this, UtsMainDosenActivity.class);
                        startActivity(intentUpdateData);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UtsUpdateDosenActivity.this, "Update Data Gagal!", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}