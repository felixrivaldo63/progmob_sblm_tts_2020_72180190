package ukdw.com.hello_world.UTS.CrudMatakuliah;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.hello_world.Model.DefaultResult;
import ukdw.com.hello_world.Network.GetDataService;
import ukdw.com.hello_world.Network.RetrofitClientInstance;
import ukdw.com.hello_world.R;
import ukdw.com.hello_world.UTS.CrudDosen.UtsDeleteDosenActivity;
import ukdw.com.hello_world.UTS.CrudDosen.UtsMainDosenActivity;

public class UtsDeleteMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cancle, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cancleCRUD:
                Intent intentCancle = new Intent(this, UtsMainMatkulActivity.class);
                startActivity(intentCancle);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_delete_matkul);

        androidx.appcompat.widget.Toolbar myToolbarMhs = (Toolbar) findViewById(R.id.toolbarDeleteMatkulUts);
        setSupportActionBar(myToolbarMhs);

        EditText edKodeDelUts = (EditText)findViewById(R.id.editTextDelMatkulUts);
        Button btnDelUts = (Button)findViewById(R.id.buttonDelMatkulUts);
        pd = new ProgressDialog(UtsDeleteMatkulActivity.this);

        edKodeDelUts.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    Toast.makeText(UtsDeleteMatkulActivity.this, "NIM Yang Akan Dihapuskan : " +
                            edKodeDelUts.getText(), Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }
        });
        btnDelUts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.setCancelable(false);
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_matkul(
                        edKodeDelUts.getText().toString(),
                        "72180190"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UtsDeleteMatkulActivity.this, "Data Dihapus!", Toast.LENGTH_LONG).show();
                        Intent intentDelData = new Intent(UtsDeleteMatkulActivity.this, UtsMainMatkulActivity.class);
                        startActivity(intentDelData);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UtsDeleteMatkulActivity.this, "Data Gagal Dihapuskan!", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}