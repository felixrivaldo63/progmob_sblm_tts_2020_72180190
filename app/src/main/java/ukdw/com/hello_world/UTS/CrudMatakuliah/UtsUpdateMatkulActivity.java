package ukdw.com.hello_world.UTS.CrudMatakuliah;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.hello_world.Model.DefaultResult;
import ukdw.com.hello_world.Network.GetDataService;
import ukdw.com.hello_world.Network.RetrofitClientInstance;
import ukdw.com.hello_world.R;
import ukdw.com.hello_world.UTS.CrudDosen.UtsMainDosenActivity;
import ukdw.com.hello_world.UTS.CrudDosen.UtsUpdateDosenActivity;
import ukdw.com.hello_world.UTS.CrudMhs.UtsMainMhsActivity;

public class UtsUpdateMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cancle, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cancleCRUD:
                Intent intentCancle = new Intent(this, UtsMainMatkulActivity.class);
                startActivity(intentCancle);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_update_matkul);

        androidx.appcompat.widget.Toolbar myToolbarMhs = (Toolbar) findViewById(R.id.toolbarUpdateMatkulUts);
        setSupportActionBar(myToolbarMhs);

        EditText edKodeCari = (EditText) findViewById(R.id.editTextKodeCariMatkulUts);
        EditText edNamaBaru = (EditText) findViewById(R.id.editTextNamaBaruMatkulUts);
        EditText edKodeBaru = (EditText) findViewById(R.id.editTextKodeBaruMatkulUts);
        EditText edHariBaru = (EditText) findViewById(R.id.editTextHariBaruMatkulUts);
        EditText edSesiBaru = (EditText) findViewById(R.id.editTextSesiBaruMatkulUts);
        EditText edSksBaru = (EditText) findViewById(R.id.editTextSksBaruMatkulUts);
        Button btnUpdate = (Button) findViewById(R.id.buttonUpdateMatkulUts);
        pd = new ProgressDialog(UtsUpdateMatkulActivity.this);
        Intent data = getIntent();

        edNamaBaru.setText(data.getStringExtra("nama"));
        edKodeBaru.setText(data.getStringExtra("kode"));
        edKodeCari.setText(data.getStringExtra("kode"));
        edHariBaru.setText(data.getStringExtra("hari"));
        edSesiBaru.setText(data.getStringExtra("sesi"));
        edSksBaru.setText(data.getStringExtra("sks"));

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Ditunggu...");
                pd.setCancelable(false);
                pd.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_matkul(
                        edNamaBaru.getText().toString(),
                        "72180190",
                        edKodeBaru.getText().toString(),
                        Integer.parseInt(edHariBaru.getText().toString()),
                        Integer.parseInt(edSesiBaru.getText().toString()),
                        Integer.parseInt(edSksBaru.getText().toString()),
                        edKodeCari.getText().toString()
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UtsUpdateMatkulActivity.this, "Data Berhasil di Ubah", Toast.LENGTH_LONG).show();
                        Intent intentUpdateData = new Intent(UtsUpdateMatkulActivity.this, UtsMainMatkulActivity.class);
                        startActivity(intentUpdateData);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UtsUpdateMatkulActivity.this, "Update Data Gagal!", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}