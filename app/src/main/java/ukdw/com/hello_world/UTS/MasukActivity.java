package ukdw.com.hello_world.UTS;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.hello_world.Crud.MahasiswaAddActivity;
import ukdw.com.hello_world.Crud.MahasiswaUpdateActivity;
import ukdw.com.hello_world.Crud.MainMhsActivity;
import ukdw.com.hello_world.MainActivity;
import ukdw.com.hello_world.Model.DefaultResult;
import ukdw.com.hello_world.Model.Dosen;
import ukdw.com.hello_world.Model.LoginRequest;
import ukdw.com.hello_world.Model.LoginResponse;
import ukdw.com.hello_world.Network.GetDataService;
import ukdw.com.hello_world.Network.RetrofitClientInstance;
import ukdw.com.hello_world.Pertemuan4.DebuggingActivity;
import ukdw.com.hello_world.R;
import ukdw.com.hello_world.Storage.SharedPrefManager;
import ukdw.com.hello_world.UTS.CrudDosen.UtsAddDosenActivity;

public class MasukActivity extends AppCompatActivity{
    ProgressDialog pd;
    private EditText etUsername;
    private EditText etPassword;
    private Button btnLogin;
    String isLogin="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_masuk);

        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);
        btnLogin = findViewById(R.id.bt_submit);
        pd = new ProgressDialog(MasukActivity.this);

        SharedPreferences prefs_file = MasukActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs_file.edit();
        isLogin = prefs_file.getString("isLogin","0");
        if(isLogin.equals("1")){
            Intent intent = new Intent(MasukActivity.this,HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin();
                editor.putString("isLogin","1");
                editor.commit();
            }
        });
    }

    private void userLogin(){
        String username = etUsername.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if(username.isEmpty()){
            etUsername.setError("Username is required");
            etUsername.requestFocus();
            return;
        }

        if(password.isEmpty()){
            etPassword.setError("Password is required");
            etPassword.requestFocus();
            return;
        }

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<LoginRequest[]> call = service.login(
                etUsername.getText().toString(),
                etPassword.getText().toString()
        );
        call.enqueue(new Callback<LoginRequest[]>() {
            @Override
            public void onResponse(Call<LoginRequest[]> call, Response<LoginRequest[]> response) {
                LoginRequest[] loginRequests = response.body();
                if (loginRequests.length > 0){
                    SharedPrefManager.getInstance(MasukActivity.this).saveUser(loginRequests[0]);
                    Toast.makeText(MasukActivity.this, "Login Berhasil" + response.message(), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(MasukActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else{
                    Toast.makeText(MasukActivity.this, "Login Failed, Periksa Kembali Username & Password Anda!" + response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginRequest[]> call, Throwable t) {
                Toast.makeText(MasukActivity.this, "Login Failed!", Toast.LENGTH_LONG).show();
            }
        });
    }
}
