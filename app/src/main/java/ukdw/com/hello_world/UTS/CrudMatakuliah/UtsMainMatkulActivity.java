package ukdw.com.hello_world.UTS.CrudMatakuliah;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.hello_world.Adapter.DosenCRUDRecyclerAdapter;
import ukdw.com.hello_world.Adapter.MatkulCRUDRecyclerAdapter;
import ukdw.com.hello_world.Model.Dosen;
import ukdw.com.hello_world.Model.Matkul;
import ukdw.com.hello_world.Network.GetDataService;
import ukdw.com.hello_world.Network.RetrofitClientInstance;
import ukdw.com.hello_world.R;
import ukdw.com.hello_world.UTS.CrudDosen.UtsAddDosenActivity;
import ukdw.com.hello_world.UTS.CrudDosen.UtsDeleteDosenActivity;
import ukdw.com.hello_world.UTS.CrudDosen.UtsMainDosenActivity;
import ukdw.com.hello_world.UTS.HomeActivity;
import ukdw.com.hello_world.UTS.MasukActivity;

public class UtsMainMatkulActivity extends AppCompatActivity {
    RecyclerView rvMatkul;
    MatkulCRUDRecyclerAdapter MatkulAdapter;
    ProgressDialog pd;
    List<Matkul> matkulList;
    String isLogin = "";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_mhs, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void logout() {
        SharedPreferences sharedPreferences = UtsMainMatkulActivity.this.getSharedPreferences("prefs_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        isLogin = sharedPreferences.getString("isLogin", "0");
        editor.putString("isLogin", "0");
        sharedPreferences.edit().clear().commit();
        Intent intent = new Intent(this, MasukActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addUts:
                Intent intentAdd = new Intent(this, UtsAddMatkulActivity.class);
                startActivity(intentAdd);
                return true;
            case R.id.delUts:
                Intent intentDel = new Intent(this, UtsDeleteMatkulActivity.class);
                startActivity(intentDel);
                return true;
            case R.id.backHomeUts:
                Intent intentBack = new Intent(this, HomeActivity.class);
                intentBack.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentBack);
                return true;
            case R.id.logoutBtn:
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        UtsMainMatkulActivity.this
                );
                builder.setIcon(R.drawable.ic_warning);
                builder.setTitle("Logout");
                builder.setMessage("Apakah Anda Ingin Logout?");

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_main_matkul);

        androidx.appcompat.widget.Toolbar myToolbarMhs = (Toolbar) findViewById(R.id.toolbarMainMatkulUts);
        setSupportActionBar(myToolbarMhs);

        rvMatkul = (RecyclerView) findViewById(R.id.rvGetMatkulAllUts);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matkul>> call = service.getMatkul("72180190");

        call.enqueue(new Callback<List<Matkul>>() {
            @Override
            public void onResponse(Call<List<Matkul>> call, Response<List<Matkul>> response) {
                pd.dismiss();
                matkulList = response.body();
                MatkulAdapter = new MatkulCRUDRecyclerAdapter(matkulList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(UtsMainMatkulActivity.this);
                rvMatkul.setLayoutManager(layoutManager);
                rvMatkul.setAdapter(MatkulAdapter);

            }

            @Override
            public void onFailure(Call<List<Matkul>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(UtsMainMatkulActivity.this, "Error", Toast.LENGTH_LONG);
            }
        });
    }
}