package ukdw.com.hello_world.UTS.CrudMhs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.hello_world.Model.DefaultResult;
import ukdw.com.hello_world.Network.GetDataService;
import ukdw.com.hello_world.Network.RetrofitClientInstance;
import ukdw.com.hello_world.R;

public class UtsUpdateMhsActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cancle, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cancleCRUD:
                Intent intentCancle = new Intent(this, UtsMainMhsActivity.class);
                startActivity(intentCancle);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_update_mhs);

        androidx.appcompat.widget.Toolbar myToolbarMhs = (Toolbar) findViewById(R.id.toolbarUpdateMhsUts);
        setSupportActionBar(myToolbarMhs);

        EditText edNimCari = (EditText) findViewById(R.id.editTextCariNimUts);
        EditText edNamaBaru = (EditText) findViewById(R.id.editTextNamaBaruUts);
        EditText edNimBaru = (EditText) findViewById(R.id.editTextNimBaruUts);
        EditText edAlamatBaru = (EditText) findViewById(R.id.editTextAlamatBaruUts);
        EditText edEmailBaru = (EditText) findViewById(R.id.editTextEmailBaruUts);
        Button btnUpdate = (Button) findViewById(R.id.buttonUpdateMahasiswaUts);
        pd = new ProgressDialog(UtsUpdateMhsActivity.this);
        Intent data = getIntent();

        edNamaBaru.setText(data.getStringExtra("nama"));
        edNimBaru.setText(data.getStringExtra("nim"));
        edNimCari.setText(data.getStringExtra("nim"));
        edAlamatBaru.setText(data.getStringExtra("alamat"));
        edEmailBaru.setText(data.getStringExtra("email"));

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Ditunggu...");
                pd.setCancelable(false);
                pd.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        edNamaBaru.getText().toString(),
                        edNimBaru.getText().toString(),
                        edNimCari.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        "kososngkan saja diisi sembarang karena dirandom sistem",
                        "72180190"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UtsUpdateMhsActivity.this, "Data Berhasil di Ubah", Toast.LENGTH_LONG).show();
                        Intent intentUpdateData = new Intent(UtsUpdateMhsActivity.this, UtsMainMhsActivity.class);
                        startActivity(intentUpdateData);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UtsUpdateMhsActivity.this, "Update Data Gagal!", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}