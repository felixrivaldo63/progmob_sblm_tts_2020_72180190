package ukdw.com.hello_world.UTS.CrudMatakuliah;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.hello_world.Model.DefaultResult;
import ukdw.com.hello_world.Network.GetDataService;
import ukdw.com.hello_world.Network.RetrofitClientInstance;
import ukdw.com.hello_world.R;
import ukdw.com.hello_world.UTS.CrudDosen.UtsAddDosenActivity;
import ukdw.com.hello_world.UTS.CrudDosen.UtsMainDosenActivity;

public class UtsAddMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cancle, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cancleCRUD:
                Intent intentCancle = new Intent(this, UtsMainMatkulActivity.class);
                startActivity(intentCancle);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_add_matkul);

        androidx.appcompat.widget.Toolbar myToolbarMhs = (Toolbar) findViewById(R.id.toolbarAddMatkulUts);
        setSupportActionBar(myToolbarMhs);

        EditText edNamaUts = (EditText) findViewById(R.id.editTextNamaMatkulUts);
        EditText edKodeUts = (EditText) findViewById(R.id.editTextKodeMatkulUts);
        EditText edHariUts = (EditText) findViewById(R.id.editTextHariMatkulUts);
        EditText edSesiUts = (EditText) findViewById(R.id.editTextSesiMatkulUts);
        EditText edSksUts = (EditText) findViewById(R.id.editTextSksMatkulUts);
        Button btnSimpanMatkulUts = (Button) findViewById(R.id.buttonSimpanMatkulUts);
        pd = new ProgressDialog(UtsAddMatkulActivity.this);

        btnSimpanMatkulUts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_matkul(
                        edNamaUts.getText().toString(),
                        "72180190",
                        edKodeUts.getText().toString(),
                        Integer.parseInt(edHariUts.getText().toString()),
                        Integer.parseInt(edSesiUts.getText().toString()),
                        Integer.parseInt(edSksUts.getText().toString())
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UtsAddMatkulActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                        Intent intentAddData = new Intent(UtsAddMatkulActivity.this, UtsMainMatkulActivity.class);
                        startActivity(intentAddData);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UtsAddMatkulActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}