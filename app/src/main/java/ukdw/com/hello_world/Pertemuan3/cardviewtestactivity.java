package ukdw.com.hello_world.Pertemuan3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.hello_world.Adapter.MahasiswaCardAdapter;
import ukdw.com.hello_world.Model.Mahasiswa;
import ukdw.com.hello_world.R;

public class cardviewtestactivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardviewtestactivity);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvGetMhsAllUts);
        MahasiswaCardAdapter MahasiswaCardAdapter;

        //Data Dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //Generate Data Mahasiswa
        Mahasiswa m1 = new Mahasiswa("Felix","72180190","081271732371");
        Mahasiswa m2 = new Mahasiswa("Elbie","72180196","082565151211");
        Mahasiswa m3 = new Mahasiswa("Evi","72180180","088545523321");
        Mahasiswa m4 = new Mahasiswa("Nadia","72180200","081545648952");
        Mahasiswa m5 = new Mahasiswa("Ficko","72180500","081548895621");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        MahasiswaCardAdapter = new MahasiswaCardAdapter(cardviewtestactivity.this);
        MahasiswaCardAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(cardviewtestactivity.this));
        rv.setAdapter(MahasiswaCardAdapter);
    }
}