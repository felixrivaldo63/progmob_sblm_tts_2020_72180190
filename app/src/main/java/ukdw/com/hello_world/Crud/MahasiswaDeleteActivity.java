package ukdw.com.hello_world.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.hello_world.Model.DefaultResult;
import ukdw.com.hello_world.Network.GetDataService;
import ukdw.com.hello_world.Network.RetrofitClientInstance;
import ukdw.com.hello_world.R;

public class MahasiswaDeleteActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_delete);

        EditText edNimDel = (EditText)findViewById(R.id.editTextDelMhs);
        Button btnDel = (Button)findViewById(R.id.buttonDelMhs);
        pd = new ProgressDialog(MahasiswaDeleteActivity.this);

        edNimDel.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    Toast.makeText(MahasiswaDeleteActivity.this, "NIM Yang Akan Dihapuskan : " +
                            edNimDel.getText(), Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }
        });

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.setCancelable(false);
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_mhs(
                        edNimDel.getText().toString(),
                        "72180190"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaDeleteActivity.this, "Data Dihapus!", Toast.LENGTH_LONG).show();
                        Intent intentDelData = new Intent(MahasiswaDeleteActivity.this, MainMhsActivity.class);
                        startActivity(intentDelData);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaDeleteActivity.this, "Data Gagal Dihapuskan!", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}