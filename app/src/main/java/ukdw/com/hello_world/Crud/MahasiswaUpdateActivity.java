package ukdw.com.hello_world.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.hello_world.Model.DefaultResult;
import ukdw.com.hello_world.Network.GetDataService;
import ukdw.com.hello_world.Network.RetrofitClientInstance;
import ukdw.com.hello_world.R;

public class MahasiswaUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        EditText edNimCari = (EditText) findViewById(R.id.editTextCariNim);
        EditText edNamaBaru = (EditText) findViewById(R.id.editTextNamaBaru);
        EditText edNimBaru = (EditText) findViewById(R.id.editTextNimBaru);
        EditText edAlamatBaru = (EditText) findViewById(R.id.editTextAlamatBaru);
        EditText edEmailBaru = (EditText) findViewById(R.id.editTextEmailBaru);
        Button btnUpdate = (Button) findViewById(R.id.buttonUpdateMahasiswa);
        pd = new ProgressDialog(MahasiswaUpdateActivity.this);
        Intent data = getIntent();

        edNamaBaru.setText(data.getStringExtra("nama"));
        edNimBaru.setText(data.getStringExtra("nim"));
        edNimCari.setText(data.getStringExtra("nim"));
        edAlamatBaru.setText(data.getStringExtra("alamat"));
        edEmailBaru.setText(data.getStringExtra("email"));

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Ditunggu...");
                pd.setCancelable(false);
                pd.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        edNamaBaru.getText().toString(),
                        edNimBaru.getText().toString(),
                        edNimCari.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        "kososngkan saja diisi sembarang karena dirandom sistem",
                        "72180190"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "Data Berhasil di Ubah", Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "Update Data Gagal!", Toast.LENGTH_LONG).show();
                    }
                });
                Intent intentUpdateData = new Intent(MahasiswaUpdateActivity.this, MainMhsActivity.class);
                startActivity(intentUpdateData);
            }
        });
    }
}